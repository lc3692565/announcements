August 14, 2021

# Release announcement: Bitcoin Cash Node v23.1.0

The Bitcoin Cash Node (BCHN) project is pleased to announce its minor release
version 23.1.0.

This release implements some interface enhancements and includes a number of
corrections and performance improvements. Interface changes are fully backward compatible
and require no adaptation of other systems.

Users who are running any of our previous releases are recommended to upgrade
to v23.1.0.

For the full release notes, please visit:

  https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v23.1.0

Executables and source code for supported platforms are available at the
above link, or via the download page on our project website at

  https://bitcoincashnode.org

We hope you enjoy our latest release and invite you to join us to improve
Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.
