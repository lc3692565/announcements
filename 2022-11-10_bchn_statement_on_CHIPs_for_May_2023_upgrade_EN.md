November 10, 2022

# BCHN Statement on CHIPs for May 2023 network upgrade

Bitcoin Cash Node, as one of many stakeholder projects in the BCH
ecosystem, has participated together with many others in the community
in a constructive process of shaping a range of Cash Improvement
Proposals (CHIPs) for the May 2023 network upgrade.

As a project, we would like to formally declare our support for the
following CHIPs to be included in the May 2023 consensus changes:

- CHIP-2021-01 Restrict Transaction Version [1]
- CHIP-2021-01 Minimum Transaction Size [2]
- CHIP-2022-02 CashTokens [3]
- CHIP-2022-05 P2SH32 [4]

Support for the activation of these CHIPs [5] will be included in our
next major release (v25.0.0).

We invite everyone who is interested in the future of Bitcoin Cash to
review and comment on the implementation of these changes ahead of
deployment in May, and to make good use of the 6 months available until
then for testing on the 'chipnet' test network [6] that will provide
a testing ground for these CHIPs from 15 November 2022 onwards.

Sincerely,

The Bitcoin Cash Node team.

---

References:

[1] https://gitlab.com/bitcoin.cash/chips/-/blob/3b0e5d55e1e139046794e850287b7acb795f4e66/CHIP-2021-01-Restrict%20Transaction%20Versions.md
    See discussion here: https://bitcoincashresearch.org/t/restrict-transaction-version-numbers/

[2] https://gitlab.com/bitcoin.cash/chips/-/blob/00e55fbfdaacf1436e455289086d9b4c6b3e7306/CHIP-2021-01-Allow%20Smaller%20Transactions.md
    See discussion here: https://bitcoincashresearch.org/t/chip-2021-01-allow-transactions-to-be-smaller-in-size/

[3] https://github.com/bitjson/cashtokens
    See discussion here: https://bitcoincashresearch.org/t/chip-2022-02-cashtokens-token-primitives-for-bitcoin-cash/

[4] https://gitlab.com/0353F40E/p2sh32/-/blob/f58ecf835f58555c9087c53af25da92a0e74534c/CHIP-2022-05_Pay-to-Script-Hash-32_(P2SH32)_for_Bitcoin_Cash.md
    See discussion here: https://bitcoincashresearch.org/t/chip-2022-05-pay-to-script-hash-32-p2sh32-for-bitcoin-cash/

[5] https://gitlab.com/im_uname/cash-improvement-proposals/-/blob/master/CHIPs.md
    See discussion here: https://bitcoincashresearch.org/t/chips-a-more-detailed-process-recommendation/

[6] https://bitcoincashresearch.org/t/staging-chips-on-testnet/
